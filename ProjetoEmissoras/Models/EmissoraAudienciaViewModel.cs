﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjetoEmissoras.Models
{
    public class EmissoraAudienciaViewModel
    {
        public Emissoras Emissoras { get; set; }
        public List<Audiencia> Audiencia { get; set; }
    }
}