﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ProjetoEmissoras.Models.Context
{
    public class Context : DbContext
    {
        public Context() : base("DefaultConnection")
        {
            Database.CreateIfNotExists();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Configure only one property 
            modelBuilder.Entity<Audiencia>()
                .Property(e => e.Data_Hora_Audiencia)
                .HasColumnType("datetime2");


            //or configure all DateTime Preperties globally(EF 6 and Above)
            modelBuilder.Properties<DateTime>()
                .Configure(c => c.HasColumnType("datetime2"));

        }

        public DbSet<Emissoras> DW_Emissoras { get; set; }
        public DbSet<Audiencia> DW_Audiencia { get; set; }
    }
}