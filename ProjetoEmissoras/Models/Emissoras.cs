﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjetoEmissoras.Models
{
    public class Emissoras : BaseModel
    {
        [StringLength(200)]
        [Index(IsUnique = true)]
        public string Nome { get; set; }
    }
}