﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ProjetoEmissoras.Models
{
    public class Audiencia : BaseModel
    {
        public int Pontos_Audiencia { get; set; }
        public DateTime Data_Hora_Audiencia { get; set; }
        public string Emissora_Audiencia { get; set; }

        [ForeignKey("Emissora")]
        public int IdEmissora { get; set; }
        public virtual Emissoras Emissora { get; set; }
    }
}