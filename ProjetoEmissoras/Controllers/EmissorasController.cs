﻿using ProjetoEmissoras.Models;
using ProjetoEmissoras.Models.Context;
using ProjetoEmissoras.Services.Interfaces;
using ProjetoEmissoras.Services.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjetoEmissoras.Controllers
{
    [Route("Emissoras")]
    public class EmissorasController : Controller
    {
        readonly IBaseRepository<Emissoras> emissoras;
        readonly IBaseRepository<Audiencia> audiencia;
        readonly AudienciaRepository audiencias;

        public EmissorasController()
        {
            this.emissoras = new BaseRepository<Emissoras>(new Context());
            this.audiencia = new BaseRepository<Audiencia>(new Context());
            this.audiencias = new AudienciaRepository(new Context());
        }

        // GET: Emissoras
        [Route("")]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [Route("detalhes")]
        public JsonResult Detalhes(int id)
        {
            Emissoras e = emissoras.GetById(id);
            List<Audiencia> a = audiencias.GetByForeignKey(id);
            EmissoraAudienciaViewModel ea = new EmissoraAudienciaViewModel() { Emissoras = e, Audiencia = a };
            var retorno = Json(ea, JsonRequestBehavior.AllowGet);
            return retorno;
        }


        [HttpPost]
        [Route("excluir")]
        public JsonResult Excluir(int id)
        {
            bool excluido = false;
            List<Audiencia> listaAudiencia = audiencias.GetByForeignKey(id);
            if (listaAudiencia != null)
            {
                foreach (Audiencia item in listaAudiencia)
                    audiencia.Delete(item);
            }

            emissoras.Delete(id);
            emissoras.Save();
            excluido = true;
            var retorno = Json(excluido, JsonRequestBehavior.AllowGet);
            return retorno;
        }

        [HttpPost]
        [Route("getallemissoras")]
        public JsonResult GetAllEmissoras()
        {
            IList<Emissoras> e = emissoras.GetAll();
            var retorno = Json(e, JsonRequestBehavior.AllowGet);
            return retorno;
        }

        [HttpPost]
        [Route("insertemissora")]
        public JsonResult InsertEmissora(Emissoras req)
        {
            if (req.Nome != null)
            {
                emissoras.Insert(req);
                emissoras.Save();
            }

            var retorno = Json(req.Id, JsonRequestBehavior.AllowGet);
            return retorno;
        }

        [HttpPost]
        [Route("insertaudiencia")]
        public JsonResult InsertAudiencia(Audiencia req)
        {
            bool adicionado = false;
            IList<Audiencia> todasAudiencias = audiencia.GetAll();
            bool existeAudiencia = todasAudiencias.Any(x => x.Data_Hora_Audiencia == req.Data_Hora_Audiencia);
            req.Emissora_Audiencia = emissoras.GetById(req.IdEmissora).Nome;
            if (!existeAudiencia)
            {
                audiencia.Insert(req);
                audiencia.Save();
                adicionado = true;
                return Json(adicionado, JsonRequestBehavior.AllowGet);
            }

            var retorno = Json(adicionado, JsonRequestBehavior.AllowGet);
            return retorno;
        }
    }
}