﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjetoEmissoras.Services.Interfaces
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        IList<TEntity> GetAll();
        TEntity GetById(int id);
        void Delete(int id);
        void Delete(TEntity obj);
        void Update(int id, TEntity obj);
        void Insert(TEntity obj);
        void Save();
    }
}