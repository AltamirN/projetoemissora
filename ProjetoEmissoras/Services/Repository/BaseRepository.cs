﻿using ProjetoEmissoras.Models.Context;
using ProjetoEmissoras.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ProjetoEmissoras.Services.Repository
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity: class
    {
        internal Context _context;
        internal DbSet<TEntity> dbSet;

        public BaseRepository(Context context)
        {
            this._context = context;
            this.dbSet = context.Set<TEntity>();
        }

        public virtual void Insert(TEntity obj)
        {
            dbSet.Add(obj);
        }

        public virtual IList<TEntity> GetAll()
        {
            return dbSet.ToList();
        }

        public TEntity GetById(int id)
        {
            return dbSet.Find(id);
        }

        public virtual void Delete(int id)
        {
            TEntity obj = GetById(id);
            dbSet.Remove(obj);
        }

        public virtual void Delete(TEntity obj)
        {
            if (_context.Entry(obj).State == EntityState.Detached)
                dbSet.Attach(obj);

            dbSet.Remove(obj);
        }

        public virtual void Save()
        {
            _context.SaveChanges();
        }

        public virtual void Update(int id, TEntity obj)
        {
            dbSet.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
        }

    }
}