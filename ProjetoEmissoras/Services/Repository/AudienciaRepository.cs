﻿using ProjetoEmissoras.Models;
using ProjetoEmissoras.Models.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjetoEmissoras.Services.Repository
{
    public class AudienciaRepository
    {
        internal Context _context;

        public AudienciaRepository(Context context)
        {
            this._context = context;
        }

        public List<Audiencia> GetByForeignKey(int id)
        {
            return _context.DW_Audiencia.Where(x => x.IdEmissora == id).ToList();
        }


    }
}