﻿
function CallWebMethod(SendObj) {

    //SendObj.data.__RequestVerificationToken = $('#antiForgeryToken').find('[name="__RequestVerificationToken"]').val();
    SendObj.data.__RequestVerificationToken = $('#__AjaxAntiForgeryForm input[name=__RequestVerificationToken]').val();
    var data = (SendObj.data ? SendObj.data : {});
    $.ajax({
        url: location.protocol + "//" + location.host + "/" + SendObj.method,
        type: "POST",
        data: data,
        cache: false,
        timeout: 9900000,
        async: (SendObj.async != null ? SendObj.async : true),
        success: function (respJson) {
            try {
                var a = JSON;
            } catch (e) {
                window.location.href = location.protocol + "//" + location.host + "/Account/LogOff";
            }

            if (respJson) {
                if (typeof (respJson) === "string") {
                    respJson = JSON.parse(respJson);
                }
            }
            SendObj.success(respJson);
        },
        error: (SendObj.error ? SendObj.error : function () { return false })
    });

};