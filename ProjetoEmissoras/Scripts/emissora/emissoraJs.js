﻿$(document).ready(function () {
    $('#sucesso').hide();
    $('#sucesso-aud').hide();
    $('#div-emissora').hide();

    $('#data_hora').on('change', function () {
        var data = $('#data_hora').val();
        if (data) {
            $('#adiciona-audiencia').prop("disabled", false)
        } else {
            $('#div-hora').hide();

        }
    });

    $('#adiciona-emissora').on('change', function () {
        if (!$('adiciona-emissora').is(":visible")) {
            $('#sucesso').hide();
            $('#nome-emissora').val();
        }
    })

    $('#adiciona').on('click', function () {
        var emissora = $('#nome-emissora').val();
        AdicionarEmissora(emissora);
    });

    $('#adiciona-audiencia').on('click', function () {
        var pontos = $('#pontos-audiencia').val();
        var data = $('#data_hora').val();
        var emissora = $('#emissora').val();
        AdicionarAudiencia(pontos, data, emissora);
    });

    GetAllEmissoras();
});


function AdicionarAudiencia(pontos_audiencia, data_hora_audiencia,emissora) {
    var req;

    req = {
        pontos_audiencia: pontos_audiencia,
        data_hora_audiencia: data_hora_audiencia,
        idemissora: emissora
    }

    CallWebMethod({
        method: "emissoras/insertaudiencia",
        data: { req: req },
        success: function (resp) {

            if (resp) {
                $('#sucesso-aud').show();
                window.location.reload();   
            }

        }
    });
}


function AdicionarEmissora(emissora) {
    var req;

    req = { nome: emissora }

    CallWebMethod({
        method: "emissoras/insertemissora",
        data: { req: req },
        success: function (resp) {

            if (resp) {
                $('#sucesso').show();
                window.location.reload();   

            }

        }
    });
}


function Excluir(id) {
    var req;

    req = { id: id }

    CallWebMethod({
        method: "emissoras/excluir",
        data: { id: id },
        success: function (resp) {

            if (resp != null) {
                window.location.reload();   
            }

        }
    });
}


function Detalhes(id) {
    var req;

    req = { id: id }

    CallWebMethod({
        method: "emissoras/detalhes",
        data: { id: id },
        success: function (resp) {
            if (resp != null) {
                $('#detalhe-nome-emissora').html(resp.Emissoras.Nome);
                html = "";
                resp.Audiencia.forEach(function (dados) {
                    debugger;
                    html +=
                        '<label> Pontos de Audiência: ' + dados.Pontos_Audiencia + '</label> <br/>'
                    + '<label> Data e hora da Audiência: ' + FormatIsoDateTime(dados.Data_Hora_Audiencia) + '</label> <br />';
                });

                $('#detalhe-audiencia').html(html)
            }

        }
    });
}

function GetAllEmissoras() {
    var req;

    req = {}

    CallWebMethod({
        method: "emissoras/getallemissoras",
        data: { req: req },
        success: function (resp) {

            if (resp != null) {
                var html = ""

                resp.forEach(function (dados) {
                    html +=
                        '<tr>'
                            + '<td id=' + dados.Id +'>' + dados.Id + '</td>'
                            + '<td>' + dados.Nome + '</td>'
                            + '<td>'
                                + '<button type="button" class="btn btn-secondary" id="audiencia" onclick="PreencheId(' + dados.Id + ')" value=' + dados.Id +'"><i class="fas fa-tv" data-toggle="modal" data-target="#criar-audiencia"></i></button> '
                                + '<button type="button" class="btn btn-primary" id="detalhes" onclick="Detalhes(' + dados.Id +')"><i class="far fa-eye" data-toggle="modal" data-target="#modal-detalhes"></i></button> '
                                + '<button type="button" class="btn btn-success" id="editar"><i class="fas fa-edit"></i></button> '
                                + '<button type="button" class="btn btn-danger" id="excluir" onclick="Excluir('+ dados.Id +')"><i class="far fa-trash-alt"></i></button> '
                            + '</td>'
                        +'</tr>'
                });

                $('#tbl-emissoras tbody').html(html);
            }

        }
    });
}

function PreencheId(id) {
    $('#emissora').val(id);
}


function FormatIsoDateTime(stringDate) {
    if (stringDate == "" || stringDate == null) {
        return "";
    }
    var aux = null;

    try {
        aux = stringDate.replace("\/Date(", "");
        aux = aux.replace(")\/", "");

        var date = new Date(parseInt(aux));
        var seconds = date.getSeconds().toString();
        var minutes = date.getMinutes().toString();
        var hour = date.getHours().toString();
        var day = date.getDate().toString();
        var month = (date.getMonth() + 1).toString();
        var year = date.getFullYear().toString();

        if (seconds.length == 1) { seconds = "0" + seconds };
        if (minutes.length == 1) { minutes = "0" + minutes };
        if (hour.length == 1) { hour = "0" + hour };
        if (day.length == 1) { day = "0" + day };
        if (month.length == 1) { month = "0" + month };

        return day + "/" + month + "/" + year + " " + hour + ":" + minutes + ":" + seconds;
    } catch (e) {
        return "";
    }
}