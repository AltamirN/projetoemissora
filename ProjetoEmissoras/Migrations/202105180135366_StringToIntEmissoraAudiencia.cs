﻿namespace ProjetoEmissoras.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StringToIntEmissoraAudiencia : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Audiencias", "Emissora_Audiencia", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Audiencias", "Emissora_Audiencia", c => c.Int(nullable: false));
        }
    }
}
