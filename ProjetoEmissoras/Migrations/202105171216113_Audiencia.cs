﻿namespace ProjetoEmissoras.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Audiencia : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Audiencias",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Pontos_Audiencia = c.Int(nullable: false),
                        Data_Hora_Audiencia = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        Emissora_Audiencia = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Audiencias");
        }
    }
}
