﻿namespace ProjetoEmissoras.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class adicionaForeignKey : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Audiencias", "IdEmissora", c => c.Int(nullable: false));
            CreateIndex("dbo.Audiencias", "IdEmissora");
            AddForeignKey("dbo.Audiencias", "IdEmissora", "dbo.Emissoras", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Audiencias", "IdEmissora", "dbo.Emissoras");
            DropIndex("dbo.Audiencias", new[] { "IdEmissora" });
            DropColumn("dbo.Audiencias", "IdEmissora");
        }
    }
}
