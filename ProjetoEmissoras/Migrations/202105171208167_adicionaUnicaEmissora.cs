﻿namespace ProjetoEmissoras.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class adicionaUnicaEmissora : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Emissoras", "Nome", c => c.String(maxLength: 200));
            CreateIndex("dbo.Emissoras", "Nome", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Emissoras", new[] { "Nome" });
            AlterColumn("dbo.Emissoras", "Nome", c => c.String());
        }
    }
}
